const calculateDiscount = (amount)  => {
    // if order amount  > 300, discount 10%

    if (amount  > 300){
        return amount * 10/100;
    }
    return 0;
}

module.exports = {
    calculateDiscount
}