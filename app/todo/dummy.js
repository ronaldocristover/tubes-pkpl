const todos = [
    {
        id: 1,
        text: 'Programming'
    },
    {
        id: 2,
        text: 'Coding'
    },
    {
        id: 3,
        text: 'Deploying'
    },
    {
        id: 4,
        text: 'Sleeping'
    },
];
module.exports = todos;