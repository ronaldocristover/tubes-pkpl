// Helper Unit Test
const expect = require('chai').expect;
const helper = require('../app/todo/helper');

describe('discount tests', () => {

    describe('calculate discount test', () => {
        it('should equal 0 if order cost is 100', () => {
            const orderCost = 100;
            const discount = helper.calculateDiscount(orderCost)
            expect(discount).to.equal(0);
        });

        it('discount should be 10%', () => {
            const orderCost = 1000;
            const discount = helper.calculateDiscount(orderCost)
            expect(discount).to.equal(100);
        });
    });
});